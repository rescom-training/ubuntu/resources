# Resources

Here you can access:
* [Cheat sheets](#cheat-sheets)
* [Communities](#communities)
* [Slides](assets/slides.pdf)
* [Websites](#websites)

## Cheat sheets

The following cheat sheet was produced by Lovelace-Tozer, M.:
[Cheat sheet](assets/cheat.pdf)

Once you have learnt some terminal commands, test yourself by attempting the [Command Line Match-up Activity](assets/activity.png)

## Communities

Ask Ubuntu is a question and answer site for Ubuntu users and developers: https://askubuntu.com.

There is also a Linux community which you can join [here](https://www.linux.org).

## Slides

You can download the training slides [here](assets/slides.pdf).

## Websites

### [Official Ubuntu Documentation](https://help.ubuntu.com)

Documentation developed and maintained by the Ubuntu Documentation Project.

Canonical Ltd. (2020). Official Ubuntu Documentation. Retrieved from https://help.ubuntu.com. Accessed April 2020.

### [Ubuntu Tutorials](https://ubuntu.com/tutorials)

These tutorials provide a step-by-step process to doing development and dev-ops activities on Ubuntu machines, servers or devices.

Canonical Ltd. (n.d.). Tutorials. Retrieved from https://ubuntu.com/tutorials. Accessed April 2020.

### [X2Go Instructions](https://meirian.github.io/X2Go)

Instructions on how to install X2Go and start a session.

Lovelace-Tozer, M. (2019). X2Go. Retrieved from https://meirian.github.io/X2Go. Accessed April 2020.

### [Command Line Tutorial](https://pawseysc.github.io/shell-hpc)

A short Introduction to Unix which has been tailored to give students enough Unix / command line knowledge to start using Pawsey resources.

Pawsey Supercomputing Centre. (2016). The Unix Shell: Pawsey Edition. Retrieved from https://pawseysc.github.io/shell-hpc. Accessed April 2020.
